#[macro_use]
extern crate pest_derive;

mod interpreter;
mod parser;

use crate::parser::TreveParser;

fn main() {
    let x = std::fs::read_to_string("examples/toparse.treve").unwrap();

    //let program = TreveParser::parse(Rule::program, &x).unwrap();
    //println!("{:#?}", program);
    let tree = TreveParser::parse_to_tree(&x);
    println!("{:#?}", tree);
    //interpreter::execute_tree(tree);
}
