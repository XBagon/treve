Program = {
    SOI ~
    (Line ~ NewLine)* ~
    Line? ~
    EOI
}

Line = {
    Tab* ~
    (
        Statement
    )
}

Statement = {
    Wait |
    Event |
    Assignment |
    Path
}

Wait = {
    "?" ~
    Identifier ~
    Parameters?
}

Event = {
    "!" ~
    Identifier ~
    Parameters?
}

Assignment = {
    Identifier ~
    "=" ~
    Expression
}

Path = {
    PathRoot ~ PathContinuation? |
    PathUp ~ ("/" ~ PathContinuation)? |
    PathBase ~ ("/" ~ PathContinuation)?
}

PathContinuation = {
    Unsigned ~ ("/" ~ PathContinuation)? |
    PathUp ~ Unsigned ~ ("/" ~ PathContinuation)?
}

PathUp = {
    ".." ~ Unsigned
}

PathBase = {
    "."
}

PathRoot = {
    "/"
}

Identifier = @{
    ('A'..'z') ~
    ('A'..'z'|'0'..'9')*
}

Parameters = {
    "(" ~
    (Assignment ~ ",")* ~
    Assignment? ~
    ")"
}

Expression = {
    (Literal |
    Identifier) ~
    (Operator ~
    Expression)*
}

Operator = _{
    Plus | Minus | Times | Divide
}

Plus = { "+" }
Minus = { "-" }
Times = { "*" }
Divide = { "/" }

Literal = {
    Signed |
    Unsigned
}

Signed = {
    ("-" | "+"?) ~ Unsigned
}

Unsigned = {
    ('0'..'9')+
}

NewLine = {
    "\n" |
    "\r\n"
}

Tab = {
    "\t" |
    "    "
}

WHITESPACE = _{
    " "
}