use super::*;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Identifier<'a>(&'a str);

impl<'a> Parse<'a> for Identifier<'a> {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Identifier<'a> {
        Identifier(p.as_str())
    }
}
