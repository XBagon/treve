use itertools::Itertools;
use pest::{iterators::Pair, Parser, Span};
use std::{fmt, i32, iter};
use trees::{Forest, Tree};

#[derive(Parser)]
#[grammar = "parser/grammar.pest"]
pub struct TreveParser;

const _GRAMMAR: &str = include_str!("grammar.pest");

impl TreveParser {
    pub fn parse_to_tree<'a>(input: &'a str) -> Forest<Statement<'a>> {
        let mut parsed = TreveParser::parse(Rule::Program, &input).unwrap();

        let program = parsed.next().unwrap();

        struct Line<'a> {
            depth: usize,
            pair: Pair<'a, Rule>,
        }
        let mut lines: Vec<Line> = Vec::new();

        let mut max_tabs = 0;

        for line in program.into_inner() {
            if line.as_rule() == Rule::Line {
                let mut iter = line.into_inner().peekable();

                let tabs = iter.by_ref().peeking_take_while(|i| i.as_rule() == Rule::Tab).count();

                if tabs > max_tabs {
                    max_tabs = tabs;
                }

                //lines.push((tabs,Token::Other(iter.next().unwrap())));
                lines.push(Line {
                    depth: tabs,
                    pair: iter.next().unwrap(),
                });
            }
        }

        let mut level_vecs: Vec<Vec<Box<Tree<Statement<'a>>>>> =
            iter::repeat_with(Vec::new).take(max_tabs + 1).collect();

        let mut scope = Scope {
            variables: HashSet::new(),
        };

        let mut last_depth = 0;

        for line in lines {
            let diff = line.depth as isize - last_depth as isize;

            if diff >= 0 {
                if diff > 1 {
                    panic!("Can only go one level deeper per line!")
                }
            } else {
                for i in (line.depth..last_depth).rev() {
                    let to_move: Vec<_> = level_vecs[i + 1].drain(..).collect();

                    for t in to_move {
                        level_vecs[i].last_mut().unwrap().push_back(*t);
                    }
                }
            }

            level_vecs[line.depth].push(Box::new(Tree::new(Statement::parse(line.pair, &scope))));

            last_depth = line.depth;
        }

        for i in (0..last_depth).rev() {
            let to_move: Vec<_> = level_vecs[i + 1].drain(..).collect();

            for t in to_move {
                level_vecs[i].last_mut().unwrap().push_back(*t);
            }
        }

        let mut tree = Forest::new();

        let forest_childs = level_vecs[0].drain(..);

        for r in forest_childs {
            tree.push_back(*r);
        }

        tree
    }
}

mod assignment;
mod decrement;
mod event;
mod expression;
mod identifier;
mod increment;
mod literals;
mod parameters;
mod path;
mod wait;
pub use self::{
    assignment::Assignment, decrement::Decrement, event::Event, expression::Expression, identifier::Identifier,
    increment::Increment, literals::*, parameters::Parameters, path::Path, wait::Wait,
};
use std::collections::HashSet;

#[derive(Debug)]
pub enum Statement<'a> {
    Wait(Wait<'a>),
    Event(Event<'a>),
    Assignment(Assignment<'a>),
    Path(Path<'a>),
    Other(Pair<'a, Rule>),
}

impl<'a> Statement<'a> {
    fn parse(p: Pair<'a, Rule>, scope: &Scope) -> Statement<'a> {
        let inner_pair = p.into_inner().next().unwrap();
        match inner_pair.as_rule() {
            Rule::Wait => Statement::Wait(Wait::parse(inner_pair, scope)),
            Rule::Event => Statement::Event(Event::parse(inner_pair, scope)),
            Rule::Assignment => Statement::Assignment(Assignment::parse(inner_pair, scope)),
            Rule::Path => Statement::Path(Path::parse(inner_pair, scope)),
            //_ => Token::Other(p),
            _ => panic!("Tried to parse unimplemented Token: {:?}", inner_pair),
        }
    }
}

struct Scope<'a> {
    variables: HashSet<Identifier<'a>>,
}

trait Parse<'a> {
    fn parse<'i: 'a>(p: Pair<'i, Rule>, scope: &Scope) -> Self;
}
