use super::*;

#[derive(Debug)]
pub struct Path<'a> {
    span: Span<'a>,
    parts: Vec<PathParts>,
}

impl<'a> Parse<'a> for Path<'a> {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Path<'a> {
        let span = p.clone().into_span();

        let mut inner = p.into_inner();

        let mut parts = Vec::new();

        let first = inner.next().unwrap();

        match first.as_rule() {
            Rule::PathBase => parts.push(PathParts::Base),
            Rule::PathUp => parts.push(PathParts::Up(first.into_inner().next().unwrap().as_str().parse().unwrap())),
            Rule::PathRoot => parts.push(PathParts::Root),
            _ => panic!(),
        }

        for i in inner {
            match i.as_rule() {
                Rule::Unsigned => parts.push(PathParts::Down(i.into_inner().next().unwrap().as_str().parse().unwrap())),
                Rule::PathUp => parts.push(PathParts::Up(i.into_inner().next().unwrap().as_str().parse().unwrap())),
                _ => panic!(),
            }
        }

        Path {
            span,
            parts,
        }
    }
}

#[derive(Debug)]
pub enum PathParts {
    Down(u32),
    Up(u32),
    Base,
    Root,
}
