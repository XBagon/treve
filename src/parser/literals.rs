use super::*;

#[derive(Debug)]
pub enum Literal {
    Unsigned(Unsigned),
    Signed(Signed),
}

#[derive(Debug)]
pub struct Unsigned(u32);

#[derive(Debug)]
pub struct Signed(i32);

impl<'a> Parse<'a> for Literal {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Literal {
        let inner_pair = p.clone().into_inner().next().unwrap();

        if inner_pair.as_rule() == Rule::Unsigned {
            Literal::Unsigned(Unsigned(inner_pair.as_str().parse().unwrap()))
        } else {
            Literal::Signed(Signed(p.as_str().parse().unwrap()))
        }
    }
}

impl<'a> Parse<'a> for Signed {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Signed {
        let s = p.as_str();

        Signed(s.parse().unwrap())
    }
}

impl<'a> Parse<'a> for Unsigned {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Unsigned {
        let s = p.as_str();

        Unsigned(s.parse().unwrap())
    }
}
