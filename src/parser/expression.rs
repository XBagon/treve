use super::*;
use pest::prec_climber::*;

#[derive(Debug)]
pub enum Expression<'a> {
    Literal(Literal),
    Identifier(Identifier<'a>),
    Addition(Addition<'a>),
    Subtraction(Subtraction<'a>),
    Multiplication(Multiplication<'a>),
    Division(Division<'a>),
}

#[derive(Debug)]
pub struct Addition<'a> {
    augend: Box<Expression<'a>>,
    addend: Box<Expression<'a>>,
}

#[derive(Debug)]
pub struct Subtraction<'a> {
    minuend: Box<Expression<'a>>,
    subtrahend: Box<Expression<'a>>,
}

#[derive(Debug)]
pub struct Multiplication<'a> {
    multiplier: Box<Expression<'a>>,
    multiplicand: Box<Expression<'a>>,
}

#[derive(Debug)]
pub struct Division<'a> {
    dividend: Box<Expression<'a>>,
    divisor: Box<Expression<'a>>,
}

impl<'a> Parse<'a> for Expression<'a> {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Expression<'a> {
        let climber = PrecClimber::new(vec![
            Operator::new(Rule::Plus, Assoc::Left) | Operator::new(Rule::Minus, Assoc::Left),
            Operator::new(Rule::Times, Assoc::Left) | Operator::new(Rule::Divide, Assoc::Left),
        ]);
        primary(p, scope, &climber)
    }
}

fn primary<'a>(p: Pair<'a, Rule>, scope: &Scope, climber: &PrecClimber<Rule>) -> Expression<'a> {
    match p.as_rule() {
        Rule::Expression => climber.climb(
            p.into_inner(),
            |pair| primary(pair, scope, climber),
            |lhs, op, rhs| match op.as_rule() {
                Rule::Plus => Expression::Addition(Addition {
                    augend: Box::new(lhs),
                    addend: Box::new(rhs),
                }),
                Rule::Minus => Expression::Subtraction(Subtraction {
                    minuend: Box::new(lhs),
                    subtrahend: Box::new(rhs),
                }),
                Rule::Times => Expression::Multiplication(Multiplication {
                    multiplier: Box::new(lhs),
                    multiplicand: Box::new(rhs),
                }),
                Rule::Divide => Expression::Division(Division {
                    dividend: Box::new(lhs),
                    divisor: Box::new(rhs),
                }),
                _ => unreachable!(),
            },
        ),
        Rule::Literal => Expression::Literal(Literal::parse(p, scope)),
        Rule::Identifier => Expression::Identifier(Identifier::parse(p, scope)),
        _ => unimplemented!(),
    }
}
