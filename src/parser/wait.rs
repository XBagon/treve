use super::*;

#[derive(Debug)]
pub struct Wait<'a> {
    span: Span<'a>,
    ident: Identifier<'a>,
    pars: Option<Parameters<'a>>,
}

impl<'a> Parse<'a> for Wait<'a> {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Wait<'a> {
        let span = p.clone().into_span();

        let mut inner = p.into_inner();

        let ident = Identifier::parse(inner.next().unwrap(), scope);
        let pars = inner.next().map(|p| Parameters::parse(p, scope));

        Wait {
            span,
            ident,
            pars,
        }
    }
}
