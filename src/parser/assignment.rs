use super::*;

#[derive(Debug)]
pub struct Assignment<'a> {
    ident: Identifier<'a>,
    expr: Expression<'a>,
}

impl<'a> Parse<'a> for Assignment<'a> {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Assignment<'a> {
        //if p.as_span().start() > 10 {panic!("{:?}",p)}
        let mut inner = p.into_inner();

        let ident = Identifier::parse(inner.next().unwrap(), scope);
        let expr = Expression::parse(inner.next().unwrap(), scope);

        Assignment {
            ident,
            expr,
        }
    }
}
