use super::*;

#[derive(Debug)]
pub struct Parameters<'a> {
    span: Span<'a>,
    data: Vec<Assignment<'a>>,
}

impl<'a> Parse<'a> for Parameters<'a> {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Parameters<'a> {
        let span = p.clone().into_span();

        let inner = p.into_inner();

        let data = inner.take_while(|p| p.as_rule() == Rule::Assignment).map(|a| Assignment::parse(a, scope)).collect();

        Parameters {
            span,
            data,
        }
    }
}
