use super::*;
use pest::error::*;
use std::process;

#[derive(Debug)]
pub struct Decrement<'a> {
    ident: Identifier<'a>,
    value: Unsigned,
}

impl<'a> Parse<'a> for Decrement<'a> {
    fn parse<'i: 'a>(p: Pair<'a, Rule>, scope: &Scope) -> Decrement<'a> {
        let span = p.clone().into_span();

        let mut inner = p.into_inner();

        let ident_rule = inner.next().unwrap();
        let ident = Identifier::parse(ident_rule.clone(), scope);
        if !scope.variables.contains(&ident) {
            eprintln!(
                "{}",
                Error::<Rule>::new_from_span(
                    ErrorVariant::CustomError {
                        message: String::from("unknown variable")
                    },
                    ident_rule.as_span()
                )
            );
            process::exit(-1);
        }
        let value = Unsigned::parse(inner.next().unwrap(), scope);

        Decrement {
            ident,
            value,
        }
    }
}
