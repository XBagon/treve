use crate::parser::*;
use rayon::iter::IntoParallelRefIterator;
use std::sync::{Arc, Mutex};
use trees::{Forest, Node};

pub fn execute_tree(tree: Forest<Statement<'_>>) {
    for e in tree_to_execution_iterator(tree) {}
}

fn tree_to_execution_iterator(tree: Forest<Statement<'_>>) -> ExecutionIterator<'_> {
    ExecutionIterator {
        active_trees: tree.iter().collect(),
        env: Arc::new(Mutex::new(Environment {})),
    }
}

struct ExecutionIterator<'a> {
    active_trees: Vec<&'a Node<Statement<'a>>>,
    env: Arc<Mutex<Environment>>,
}

impl<'a> Iterator for ExecutionIterator<'a> {
    type Item = Vec<&'a Node<Statement<'a>>>;

    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        //self.active_trees.iter().map(|n|n.data).collect::<Vec<_>>().par_iter().for_each(|t| t.execute(self.env.clone()).unwrap());

        //self.active_trees
        unimplemented!()
    }
}

struct Environment {}

#[derive(Debug)]
enum ExecutionResult {
    Ok,
}

#[derive(Debug)]
enum ExecutionError {}

impl<'a> Statement<'a> {
    fn execute(&self, env: Arc<Mutex<Environment>>) -> Result<ExecutionResult, ExecutionError> {
        match self {
            Statement::Wait(a) => a.execute(env),
            Statement::Event(a) => a.execute(env),
            Statement::Assignment(a) => a.execute(env),
            Statement::Path(a) => a.execute(env),
            _ => panic!(),
        }
    }
}

trait Execute
where
    Self: std::fmt::Debug,
{
    fn execute(&self, env: Arc<Mutex<Environment>>) -> Result<ExecutionResult, ExecutionError> {
        println!("Executing: {:?}", self);
        Ok(ExecutionResult::Ok)
    }
}

impl<'a> Execute for Wait<'a> {
    fn execute(&self, _env: Arc<Mutex<Environment>>) -> Result<ExecutionResult, ExecutionError> {
        unimplemented!()
    }
}

impl<'a> Execute for Event<'a> {
    fn execute(&self, _env: Arc<Mutex<Environment>>) -> Result<ExecutionResult, ExecutionError> {
        unimplemented!()
    }
}

impl<'a> Execute for Assignment<'a> {
    fn execute(&self, _env: Arc<Mutex<Environment>>) -> Result<ExecutionResult, ExecutionError> {
        unimplemented!()
    }
}

impl<'a> Execute for Increment<'a> {
    fn execute(&self, _env: Arc<Mutex<Environment>>) -> Result<ExecutionResult, ExecutionError> {
        unimplemented!()
    }
}

impl<'a> Execute for Decrement<'a> {
    fn execute(&self, _env: Arc<Mutex<Environment>>) -> Result<ExecutionResult, ExecutionError> {
        unimplemented!()
    }
}

impl<'a> Execute for Path<'a> {
    fn execute(&self, _env: Arc<Mutex<Environment>>) -> Result<ExecutionResult, ExecutionError> {
        unimplemented!()
    }
}
